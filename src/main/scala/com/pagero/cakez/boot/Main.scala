package com.pagero.cakez.boot

import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy.Restart
import akka.routing.RoundRobinPool
import com.pagero.cakez.actors.InputReader.Input
import com.pagero.cakez.actors.{CakezActorSystem, InputReader}
import com.pagero.cakez.util.SenzLogger

/**
  * We do start InputReader actor from here
  *
  * @author eranga bandara(erangaeb@gmail.com)
  */
object Main extends App with CakezActorSystem with SenzLogger {
  logger.debug("Booting application")

  // start input reader actor
  //  val inputReader = system.actorOf(Props(classOf[InputReader]), name = "InputRender")
  //  inputReader ! InitReader

  val msgHandler = system.actorOf(RoundRobinPool(
    nrOfInstances = 2,
    supervisorStrategy = OneForOneStrategy(loggingEnabled = false) {
      case e =>
        logError(e)
        Restart
    }
  ).props(InputReader.props()))

  def ss = OneForOneStrategy(loggingEnabled = false) {
    case e =>
      logError(e)
      Restart
  }

  while (true) {
    println()
    println()
    println("--------------------------------------------")
    println("ENTER #Employee[id name department]")
    println("--------------------------------------------")
    println()

    // read command line input
    val inputEmp = scala.io.StdIn.readLine()

    logger.debug(s"Read input employee $inputEmp")

    msgHandler ! Input(inputEmp)

  }

}
