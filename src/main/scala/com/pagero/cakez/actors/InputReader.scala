package com.pagero.cakez.actors

import akka.actor.{Actor, Props}
import com.pagero.cakez.actors.InputReader.Input
import org.slf4j.LoggerFactory

case class InitReader()

object InputReader {

  case class Input(msg: String)

  def props() = Props(classOf[InputReader])

}

/**
  * Actor class which handles reading inputs form the commandline
  * Starts to work on 'InitReader' message
  *
  * @author eranga bandara(erangaeb@gmail.com)
  */
class InputReader extends Actor {

  def logger = LoggerFactory.getLogger(this.getClass)

  override def preStart: Unit = {
    logger.debug(s"Starting Actor .... ${context.self.path}")
  }

  // employee handler dependencies via trait mixing
  //trait EmployeeHandlerConfig extends SprayUserServiceComp with CakezActorSystem with CassandraEmployeeDbComp with CakezCassandraCluster

  //val employeeHandler = new EmployeeHandler with EmployeeHandlerConfig

  override def receive: Receive = {
    case InitReader =>
      //      while (true) {
      //        println()
      //        println()
      //        println("--------------------------------------------")
      //        println("ENTER #Employee[id name department]")
      //        println("--------------------------------------------")
      //        println()
      //
      //        // read command line input
      //        val inputEmp = scala.io.StdIn.readLine()
      //
      //        logger.debug(s"Read input employee ${inputEmp}")
      //
      //        //handleInput(inputEmp)
      //      }

      logger.debug(s"init reader .... ")
    case Input(msg) =>
      logger.debug(s"input  .... $msg ")

      if (msg.equalsIgnoreCase("java"))
        throw new IllegalArgumentException("hoooo")
  }

  //  def handleInput(inputEmp: String) = {
  //    // handle employee via Employee handler
  //    try {
  //      employeeHandler.createEmployee(inputEmp) match {
  //        case Employee(id, name, _) =>
  //          println("[DONE] employee created with name " + name)
  //        case _ =>
  //          println("[FAIL] employee creation failed")
  //      }
  //    } catch {
  //      case e: InvalidEmployeeInput =>
  //        logger.error("error ", e)
  //        println(s"[FAIL] ${e.msg}")
  //      case e: InvalidEmployeeId =>
  //        logger.error("error ", e)
  //        println(s"[FAIL] ${e.msg}")
  //      case e: Throwable =>
  //        logger.error("error ", e)
  //        println(e.getStackTrace)
  //    }
  //  }
}